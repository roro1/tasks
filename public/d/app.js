const taskList = document.querySelector('#task-list');
const form = document.querySelector('#add-task-form');
const modalForm = document.querySelector('#modal-form');
const modeldeel1 = document.getElementById("modaldeel1");

var xtasks =  []
var inst = {
  tekstlen: 40,
  mode: "active", // hidded
  extra: "Doe"
};

var liInModal = 0;
//hideAddressBar();  //for mobile browsers

function menuFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

function evalInput(tkst){
  var d2 = waitDays(0);
  var inp = { tekst: "", cat: "", prio: 2, hideUntil: d2}
  var comando = "";
  var i;
  var ii = 0;
  for (i = tkst.length-1; i > tkst.length-5; i--) {
    if (tkst[i] == ',') { ii=i };
  };
  if (ii == 0){
    inp.tekst = tkst;
  } else {
    comando = tkst.slice(ii+1,str.length);
    inp.tekst = tkst.slice(0,ii);
    }
  console.log(comando);
  if ( comando.includes("w")) {inp.cat ="w"}
  if ( comando.includes("p")) {inp.cat ="p"}
  if ( comando.includes("m")) {inp.cat ="m"}

  if ( comando.includes("=")) {inp.prio =4}
  if ( comando.includes("+")) {inp.prio =4}
  if ( comando.includes("-")) {inp.prio = 1}
  if ( comando.includes("1")) {inp.hideUntil = waitDays(1)}
  if ( comando.includes("2")) {inp.hideUntil = waitDays(2)}
  if ( comando.includes("3")) {inp.hideUntil = waitDays(3)}
  if ( comando.includes("4")) {inp.hideUntil = waitDays(4)}
  if ( comando.includes("5")) {inp.hideUntil = waitDays(5)}
  if ( comando.includes("6")) {inp.hideUntil = waitDays(6)}
  if ( comando.includes("7")) {inp.hideUntil = waitDays(7)}
  if ( comando.includes("8")) {inp.hideUntil = waitDays(8)}
  if ( comando.includes("9")) {inp.hideUntil = waitDays(9)}

  console.log("aan einde eval tekst: ", inp.tekst," cat: ",inp.cat," prio: ",inp.prio)
  return inp
}

function waitDays(days){
  var dd = new Date();
  dd.setDate(dd.getDate() + days);
  var jaar = dd.getFullYear();
  var maand = dd.getMonth();
  var dag = dd.getDate();
  var result = new Date(jaar, maand, dag, 8, 00);
  //console.log("nieuwe hide date: ",result)
  return result;
}

function addItem(li,welk,tekst) {
  if (welk =="wachtreeks") {
    var wachtknop = [14,7,6,5,4,3,2,1];
    var i;
    var textt;
    var d = new Date();
    var dd =d.getDay();
    var days = ["zo","ma","di","wo","do","vr","za"];
    for (i = 0; i < wachtknop.length; i++) {
      if (wachtknop[i]< 8){
        ddd = dd + wachtknop[i];
        if (ddd >6) { ddd = ddd - 7};
        textt = days[ddd];
      } else {textt ='++'}
      knn = "ww"+wachtknop[i].toString();
      addItem(li,knn,textt);

    }
  }
  else {
  let kn = document.createElement('span');
  kn.classList.add(welk,"knop");
  if ((tekst === undefined))  {  //dan is het een material icon
    kn.classList.add("material-icons")
//      if (welk == "wacht"){ kn.textContent = 'schedule'; }
//      if (welk == "wacht7"){ kn.textContent = 'alarm_add'; }
    if (welk == "prio"){ kn.textContent = 'arrow_upward'; }
    if (welk == "prioDown"){ kn.textContent = 'arrow_downward'; }
    if (welk == "verwijder"){ kn.textContent = 'check_circle_outline';}
//      if (welk == "extra"){ kn.textContent = 'build';}
//      if (welk == "extra2"){ kn.textContent = 'work_outline';}
    if (welk == "prive"){ kn.textContent = 'home';}
    if (welk == "werk"){ kn.textContent = 'work_outline';}
    if (welk == "maybe"){ kn.textContent = 'help_outline';}

    if (welk == "queue"){ kn.textContent = 'add_to_queue';}
    } else {kn.textContent = tekst}
  li.appendChild(kn);
  kn.addEventListener('click', (e) => {klikknop(e);  });
}
}

function klikknop(e){    // afhandellen event
  e.stopPropagation();
  let parent = e.target.parentElement.nodeName;
  var liid = liInModal;  //indien knop in modal
  if (parent =="LI") { liid = e.target.parentElement.id;} //indien knop in LI
  let fsid = xtasks[liid].fsid;
  let knop = e.target.classList.item(0)
  console.log("klikknop: ", knop,"parent: ",parent," liid: ",liid, "fsid:",fsid);

  if (knop.substring(0,2) == "ww"){
    var dd2 = waitDays(parseInt(knop.substring(2)) );
    console.log(" knop ww hide until", dd2);
    timest = firebase.firestore.Timestamp.fromDate(dd2);
    db.collection('tasks').doc(fsid).update({hideUntil : timest});
    modal.style.display = "none"; // sluit modal

  }


  switch(knop) {
      case 'verwijder':
      console.log("delete", liid);
      db.collection('tasks').doc(fsid).delete();
      break;
    case 'prio':
      xtasks[liid].prio ++;
      console.log("prio wordt: ",xtasks[liid].prio);
      db.collection('tasks').doc(fsid).update({prio : xtasks[liid].prio});
      break;
    case 'prioDown':
      console.log("prioDown");
      if (xtasks[liid].prio >0) {xtasks[liid].prio --}
      console.log("prio wordt: ",xtasks[liid].prio);
      db.collection('tasks').doc(fsid).update({prio : xtasks[liid].prio});
      break;
    case 'werk':
      db.collection('tasks').doc(fsid).update({cat : 'w'});
      break;
    case 'prive':
      db.collection('tasks').doc(fsid).update({cat : 'p'});
      break;
    case 'maybe':
      db.collection('tasks').doc(fsid).update({cat : 'm'});
      break;
    case 'queue':
      db.collection('tasks').doc(fsid).update({cat : 'q'});
      break;

    case 'tekst':
      console.log("op tekst geklikt ");
      askModal(liid);
      break;
  }
}

function renderTask(doc){  // create element & render
    let li = document.createElement('li');
    taskList.appendChild(li);
    let task = document.createElement('span');
    task.classList.add("tekst");
    li.appendChild(task);
    li.setAttribute('data-id', doc.id);
    str = doc.data().task;      // voeg firestore reccord (=doc) toe aan array
    nr = -1 + xtasks.push({fsid: doc.id, tekst:str, prio: doc.data().prio, cat: doc.data().cat});
    li.id = nr.toString();  //het id van listitem = str nr element array
    xtasks[nr].hideUntil = doc.data().hideUntil.toDate();
    if (doc.data().containsKey('due')){ console.log("due veld is er"), xtasks[nr].due = doc.data().due.toDate(); } else {console.log("due veld is er NIET") }

    opmaakLi(li);

    if (screen.width >900) {str = str.slice(0,inst.tekstlen);}
    task.textContent = str;

    task.addEventListener('click', (e) => {klikknop(e);  }); //voor klik op de tekst

    addItem(li,"verwijder");
    if (screen.width >900) {
      addItem(li,"prioDown");
      addItem(li,"prio");
      addItem(li,"wachtreeks");
      addItem(li,"prive");
      addItem(li,"werk");
      addItem(li,"maybe");

    }
}
// geef li correct weer aan de hand van properties en global settings
function opmaakLi(li) {
  elem = Number(li.id)
  var d = new Date();
  var ddiv = (xtasks[elem].hideUntil - d) /86400 ;
  li.style.display = "block";
  switch(inst.mode) {
    case 'active':
      if  ((xtasks[elem].cat == 'm') || (ddiv>0 )) {
        li.style.display = "none";
        //res = res + "("+Math.round(ddiv)+")";
        }
      break;
    case 'p':
      if  ((xtasks[elem].cat == 'w') || (xtasks[elem].cat == 'm') || ( ddiv>0 )) {
        li.style.display = "none";
        }
      break;
    case 'w':
      if  ((xtasks[elem].cat == 'p') || (xtasks[elem].cat == 'm') ||( ddiv>0 )) {
        li.style.display = "none";
        }
      break;
    case 'm':
      if  ((xtasks[elem].cat == 'p') || (xtasks[elem].cat == 'w') ||( ddiv>0 )) {
        li.style.display = "none";
        }
      break;

    }


  if (xtasks[elem].cat == 'w') { li.style.color = "SaddleBrown";   }
  if (xtasks[elem].cat == 'm') { li.style.color = "orange";   }
  if (xtasks[elem].cat == 'p') {li.style.color = "darkblue";}
  if (xtasks[elem].prio < 4) {li.style.backgroundColor = "White";}
  if (xtasks[elem].prio == 4) {li.style.backgroundColor = "#fff2e6";}
  if (xtasks[elem].prio == 5) {li.style.backgroundColor = "ffe6cc";}
  if (xtasks[elem].prio > 5) {li.style.backgroundColor = "#ffd9b3";}

}


function show(wat) {
inst.mode = wat;
var myElements = document.querySelectorAll("li");
var ii = myElements.length;
console.log("inst.mode wordt: ",inst.mode," aantal li elementen: ",ii);
var i;
for (i = 0; i < ii; i++) {
  opmaakLi(myElements[i]);
  }
}



function askModal(liid){
  document.getElementById("modaltekst").innerHTML = xtasks[liid].tekst
  console.log("hideUntil: ",xtasks[liid].hideUntil)
  modalForm.mtask.value = xtasks[liid].tekst ;
   document.getElementById("rDate").value = xtasks[liid].hideUntil.toISOString().substr(0, 10);
  console.log("date element rDate: ",document.getElementById("rDate").value  )
  //modalForm.birthday.value = xtasks[liid].hideUntil;

  modal.style.display = "block";
  liInModal = liid;

}



console.log("start hoofd prog build 25 juli 2022============================================");
console.log("Total Width: ", + screen.width );
// Get the modal
var modal = document.getElementById("myModal");

// knoppen toevoegen aan modal
//var modalcont = document.getElementById("modalco");
addItem(modaldeel1,"verwijder");
addItem(modaldeel1,"prioDown");
addItem(modaldeel1,"prio");
let knx3 = document.createElement('span');
knx3.innerHTML = "<BR><BR><BR>";
modaldeel1.appendChild(knx3);
addItem(modaldeel1,"prive");
addItem(modaldeel1,"werk");
addItem(modaldeel1,"queue");


let knx2 = document.createElement('span');
knx2.innerHTML = "<BR><BR><BR>";
modaldeel1.appendChild(knx2);

addItem(modaldeel1,"wachtreeks");



let knx = document.createElement('span');
knx.innerHTML = "<BR><BR><BR>";
modaldeel1.appendChild(knx);

console.log("stop knoppen toevoegen ==============================================")
firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    var displayName = user.displayName;
    var email = user.email;
    var emailVerified = user.emailVerified;
    var photoURL = user.photoURL;
    var isAnonymous = user.isAnonymous;
    var uid = user.uid;
    var providerData = user.providerData;
    console.log(" User is signed in met: ", email," En UID: ",uid);

    // ...
  } else {
    console.log(" User is signed OUT ======.");
    window.location.href ="login.html"
    // ...
  }
});


// saving data
form.addEventListener('submit', (e) => {
    e.preventDefault();
    var inpu = evalInput(form.task.value);
    console.log("inpu tekst: ",inpu.tekst ,"   cat ", inpu.cat,inpu.prio,"hide until: ",inpu.hideUntil);
    timest = firebase.firestore.Timestamp.fromDate(inpu.hideUntil);
    db.collection('tasks').add({
        task: inpu.tekst,
        timeStamp: firebase.firestore.Timestamp.now(),
        cat: inpu.cat,
        prio: inpu.prio,
        hideUntil : timest
    });
    form.task.value = '';
});

// saving data uit modal
modalForm.addEventListener('submit', (e) => {
    e.preventDefault();
    let parent = e.target.parentElement.nodeName;
    console.log("parent el: ",parent)
    var liid = liInModal;  //indien knop in modal haal waarde uit global var
    xtasks[liid].tekst = modalForm.mtask.value;
    console.log("date element rDate: ",document.getElementById("rDate").value  )
    var datv = document.getElementById("rDate").value
    var dd =new Date(datv);
    var jaar = dd.getFullYear();
    var maand = dd.getMonth();
    var dag = dd.getDate();
    var dd = new Date(jaar, maand, dag, 8, 30);
    xtasks[liid].hideUntil = new Date(dd);
    console.log("inpu tekst uit modal: ",xtasks[liid].tekst ,"   cat ", xtasks[liid].cat,xtasks[liid].prio,"hide until: ",xtasks[liid].hideUntil);
    modalForm.mtask.value = '----done-----';
    var fsid = xtasks[liInModal].fsid
    console.log( "liInModal: ",liInModal," fsid: ",fsid);


    db.collection('tasks').doc(fsid).update({
      prio : xtasks[liid].prio,
      task: xtasks[liid].tekst,
      hideUntil: xtasks[liid].hideUntil,
      cat: xtasks[liid].cat   });
});

// real-time DATABASE listener
var sorteer = "prio";
db.collection('tasks').orderBy(sorteer,"desc").onSnapshot(snapshot => {
    taskList.innerHTML = "";
    console.log("- reload-")
    xtasks =  []; //leeg tasklist
    snapshot.forEach(doc => {
      renderTask(doc);
    });
});




// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];


// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
